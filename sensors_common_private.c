/**
 * @file
 * @author Martin Stejskal
 * @brief Share internal functionality and defines
 *
 * This header should NOT be used by upper layers. Only for internal usage of
 * drivers.
 */
// ===============================| Includes |================================
#include "sensors_common_private.h"

#include <assert.h>
#include <stdbool.h>

#include "math_vector.h"
// ================================| Defines |================================
/**
 * @brief Range of raw values from magnetometer accepted when calibrating
 *
 * @{
 */
#define MAG_MAX_VALUE_WHEN_CALIBRATING (10000)
#define MAG_MIN_VALUE_WHEN_CALIBRATING (-1 * MAG_MAX_VALUE_WHEN_CALIBRATING)
/**
 * @}
 */
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *tag = "Sensors common (p)";
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
e_sensor_error sensor_check_who_am_i(const uint8_t u8_dev_addr,
                                     const uint8_t u8_reg_addr,
                                     const uint8_t u8_expected_value) {
  uint8_t u8_reg_value = 0;

  e_sensor_error e_err_code = sensor_read_reg(
      u8_dev_addr, u8_reg_addr, &u8_reg_value, sizeof(u8_reg_value));
  if (e_err_code) {
    // NACK received -> no device with such address on the bus
    SENSORS_LOGD(tag, "No device found at bus address 0x%02X", u8_dev_addr);

    e_err_code = SENSOR_DEVICE_NOT_FOUND;

  } else if (u8_reg_value != u8_expected_value) {
    // Device use that address, but register value does not match
    SENSORS_LOGD(tag,
                 "Device bus address 0x%02X -> register value does not match "
                 "(0x%02X != 0x%02X)",
                 u8_dev_addr, u8_reg_value, u8_expected_value);

    e_err_code = SENSOR_INVALID_VALUE;

  } else {
    e_err_code = SENSOR_OK;
    SENSORS_LOGD(tag, "Sensor found at address 0x%02X (reg. value: 0x%02X)",
                 u8_dev_addr, u8_reg_value);
  }

  // Make compiler happy when logging is disabled
  (void)tag;

  return e_err_code;
}

e_sensor_error sensor_check_who_am_i_list(
    const uint8_t *pu8_dev_addr_list, const uint8_t u8_addr_list_num_of_items,
    const uint8_t u8_reg_addr, const uint8_t *pu8_expected_value_list,
    const uint8_t u8_value_list_num_of_items, uint8_t *pu8_found_dev_addr,
    uint8_t *pu8_found_value) {
  // Check input arguments
  if (!pu8_dev_addr_list || !pu8_expected_value_list || !pu8_found_dev_addr) {
    return SENSOR_EMPTY_POINTER;
  }

  if ((0 == u8_addr_list_num_of_items) || (0 == u8_value_list_num_of_items)) {
    return SENSOR_INVALID_PARAM;
  }

  e_sensor_error e_err_code = SENSOR_FAIL;
  // Signal if device was at least found on the bus or not
  bool b_dev_found_at_bus = false;

  // Iterate over bus address
  for (uint8_t u8_addr_idx = 0; u8_addr_idx < u8_addr_list_num_of_items;
       u8_addr_idx++) {
    // Iterate over all expected values
    for (uint8_t u8_expect_value_idx = 0;
         u8_expect_value_idx < u8_value_list_num_of_items;
         u8_expect_value_idx++) {
      // Simplify code - store core values into separate variables
      const uint8_t u8_dev_addr = pu8_dev_addr_list[u8_addr_idx];
      const uint8_t u8_expected_value =
          pu8_expected_value_list[u8_expect_value_idx];

      // If error code is not "OK", keep searching
      e_err_code =
          sensor_check_who_am_i(u8_dev_addr, u8_reg_addr, u8_expected_value);

      // Sensor detected -> get out from loop
      if (e_err_code == SENSOR_OK) {
        // If found, always store device address
        *pu8_found_dev_addr = u8_dev_addr;

        // If pointer is not empty, write info about current value
        if (pu8_found_value) {
          *pu8_found_value = u8_expected_value;
        }

        break;
      } else if (e_err_code == SENSOR_DEVICE_NOT_FOUND) {
        // It does not make sense to keep trying this bus address. Try next one
        break;

      } else if (e_err_code == SENSOR_INVALID_VALUE) {
        b_dev_found_at_bus = true;
      } else {
        // Nothing to do. Keep it rolling
      }
    }

    // Sensor detected -> get out from loop
    if (e_err_code == SENSOR_OK) {
      break;
    }
  }

  // Return overall status
  if (e_err_code == SENSOR_OK) {
    // Nothing to do. Everything is fine
  } else if (b_dev_found_at_bus) {
    // Device was found at bus, but eventually register value did not match
    e_err_code = SENSOR_INVALID_VALUE;
  } else {
    // Some other situation. Keep what is in the error code
  }

  return e_err_code;
}
// ========================| Middle level functions |=========================
e_sensor_error sensor_calibrate_gyroscope(const tf_sensor_get_raw_cb pf_get_raw,
                                          const uint16_t u16_num_of_iterations,
                                          ts_sensor_vect *ps_correction) {
  // Input parameter check
  if ((!pf_get_raw) || (!ps_correction)) {
    return SENSOR_EMPTY_POINTER;
  }

  if (!u16_num_of_iterations) {
    return SENSOR_INVALID_PARAM;
  }

  SENSORS_LOGI(tag, "Calibrating gyroscope...");

  // Need to collect data in 32-bit long value due to add operation to
  // avoid overflow
  int32_t i32_calib_x = 0, i32_calib_y = 0, i32_calib_z = 0;

  // Store raw data from gyroscope here
  ts_sensor_utils_data s_gyro_raw;

  // For showing progress in %. First print at 10% of completed task
  uint8_t u8_perc_val = 10;

  // To avoid calculating threshold all the time (dividing), calculate it
  // in advance
  uint16_t u16_perc_show_threshold = u16_num_of_iterations / 10;

  for (uint16_t u16_cnt = 0; u16_cnt < u16_num_of_iterations; u16_cnt++) {
    SENSORS_RET_IF_ERR(pf_get_raw(SENSOR_FEATURE_GYROSCOPE, &s_gyro_raw));

    i32_calib_x += s_gyro_raw.u_d.s_vect.i16_x;
    i32_calib_y += s_gyro_raw.u_d.s_vect.i16_y;
    i32_calib_z += s_gyro_raw.u_d.s_vect.i16_z;

    if (u16_cnt >= u16_perc_show_threshold) {
      SENSORS_LOGI(tag, "%d %%", u8_perc_val);

      // Set new threshold - at next 10 %
      u16_perc_show_threshold += u16_num_of_iterations / 10;
      // Update percentage value
      u8_perc_val += 10;
    }
  }

  // Check if result is 16 bit long (should be)
  assert((i32_calib_x / u16_num_of_iterations) <= 0xFFFF);
  assert((i32_calib_y / u16_num_of_iterations) <= 0xFFFF);
  assert((i32_calib_z / u16_num_of_iterations) <= 0xFFFF);

  // Correction is inverted value -> -1 * (offset)
  ps_correction->i16_x = -1 * i32_calib_x / u16_num_of_iterations;
  ps_correction->i16_y = -1 * i32_calib_y / u16_num_of_iterations;
  ps_correction->i16_z = -1 * i32_calib_z / u16_num_of_iterations;

  SENSORS_LOGI(tag, "Calibration completed (Correction: x:%d y:%d z:%d)",
               ps_correction->i16_x, ps_correction->i16_y,
               ps_correction->i16_z);

  return SENSOR_OK;
}

e_sensor_error sensor_calibrate_magnetometer(
    const tf_sensor_get_raw_cb pf_get_raw, const uint16_t u16_num_of_iterations,
    ts_sensor_vect *ps_offset, ts_sensor_vect *ps_scale) {
  // Input parameter check
  if ((!pf_get_raw) || (!ps_offset) || (!ps_scale)) {
    return SENSOR_EMPTY_POINTER;
  }

  if (!u16_num_of_iterations) {
    return SENSOR_INVALID_PARAM;
  }

  SENSORS_LOGI(tag, "Calibrating magnetometer...");

  // Big thanks to following author:
  // https://appelsiini.net/2018/calibrate-magnetometer/
  // Originally written in Python, but well explained

  // Recorded minimum - used by "math vector" - therefore different type
  ts_mv_vector_16 s_min = {
      .i16_x = INT16_MAX, .i16_y = INT16_MAX, .i16_z = INT16_MAX};

  // Recorded maximum - used by "math vector" - therefore different type
  ts_mv_vector_16 s_max = {
      .i16_x = INT16_MIN, .i16_y = INT16_MIN, .i16_z = INT16_MIN};

  // Actual magnetic vector
  ts_sensor_utils_data s_actual;

  // Average delta vector & average over axes for soft iron correction
  ts_sensor_vect s_avg_delta;
  int16_t i16_avg_delta;

  // For some intermediate calculations is required to use more bits to
  // avoid overflow at 16 bit long variables
  int32_t i32_tmp;

  // For showing progress in %. First print at 10% of completed task
  uint8_t u8_perc_val = 10;

  // To avoid calculating threshold all the time (dividing), calculate it
  // in advance
  uint16_t u16_perc_show_threshold = u16_num_of_iterations / 10;

  for (uint16_t u16_iteration_cnt = 0;
       u16_iteration_cnt < u16_num_of_iterations; u16_iteration_cnt++) {
    // Show progress once a while
    if (u16_iteration_cnt >= u16_perc_show_threshold) {
      SENSORS_LOGI(tag, "Magnetometer calibration: %d %%", u8_perc_val);

      // Set new threshold - at next 10 %
      u16_perc_show_threshold += u16_num_of_iterations / 10;
      // Update percentage value
      u8_perc_val += 10;
    }

    // Load magnetic data only with factory calibration data
    SENSORS_RET_IF_ERR(pf_get_raw(SENSOR_FEATURE_MAGENETOMETER, &s_actual));

    // Get minimum and maximum value. Need to use "math vector" structure
    ts_mv_vector_16 s_mv_actual = {
        .i16_x = s_actual.u_d.s_vect.i16_x,
        .i16_y = s_actual.u_d.s_vect.i16_y,
        .i16_z = s_actual.u_d.s_vect.i16_z,
    };

    mv_vect_16_get_minimum(&s_min, &s_mv_actual, &s_min);
    mv_vect_16_get_maximum(&s_max, &s_mv_actual, &s_max);
  }

  // Calculate offset (hard iron correction). In general, output from
  // magnetometer should be relatively small numbers -> no need to calculate
  // it inside 32 bit variable. But check it. Just in case.
  // Note that mathematical module require bit different "vector type" -> need
  // to use that one (although they should be "same")
  ts_mv_vector_16 s_math_16 = {
      .i16_x = s_min.i16_x, .i16_y = s_min.i16_y, .i16_z = s_min.i16_z};

  if (!mv_vect_16_are_components_in_range(&s_math_16,
                                          MAG_MIN_VALUE_WHEN_CALIBRATING,
                                          MAG_MAX_VALUE_WHEN_CALIBRATING)) {
    return SENSOR_UNEXPECTED_EXTERNAL_CONDITIONS;
  }

  s_math_16.i16_x = s_max.i16_x;
  s_math_16.i16_y = s_max.i16_y;
  s_math_16.i16_z = s_max.i16_z;

  if (!mv_vect_16_are_components_in_range(&s_math_16,
                                          MAG_MIN_VALUE_WHEN_CALIBRATING,
                                          MAG_MAX_VALUE_WHEN_CALIBRATING)) {
    return SENSOR_UNEXPECTED_EXTERNAL_CONDITIONS;
  }

  // =========================| Hard iron correction |======================
  // Calculate offset: (maximum + minimum)/2
  ps_offset->i16_x = (s_max.i16_x + s_min.i16_x) >> 1;
  ps_offset->i16_y = (s_max.i16_y + s_min.i16_y) >> 1;
  ps_offset->i16_z = (s_max.i16_z + s_min.i16_z) >> 1;

  // =========================| Soft iron correction |======================
  s_avg_delta.i16_x = (s_max.i16_x - s_min.i16_x) / 2;
  s_avg_delta.i16_y = (s_max.i16_y - s_min.i16_y) / 2;
  s_avg_delta.i16_z = (s_max.i16_z - s_min.i16_z) / 2;

  i16_avg_delta =
      (s_avg_delta.i16_x + s_avg_delta.i16_y + s_avg_delta.i16_z) / 3;

  // Avoid division of zero. If for some reason average delta vector will be
  // zero, set it at least to "1". This brings small inaccuracy, but at least
  // algorithm will work instead of crash
  if (s_avg_delta.i16_x == 0) {
    s_avg_delta.i16_x = 1;
  }
  if (s_avg_delta.i16_y == 0) {
    s_avg_delta.i16_y = 1;
  }
  if (s_avg_delta.i16_z == 0) {
    s_avg_delta.i16_z = 1;
  }

  // And finally scale. Since integer can not store fractional values, result
  // is multiplied by 1000
  i32_tmp = (1000 * (int32_t)i16_avg_delta) / ((int32_t)s_avg_delta.i16_x);
  assert((i32_tmp >= INT16_MIN) && (i32_tmp <= INT16_MAX));
  ps_scale->i16_x = (int16_t)i32_tmp;

  i32_tmp = (1000 * (int32_t)i16_avg_delta) / ((int32_t)s_avg_delta.i16_y);
  assert((i32_tmp >= INT16_MIN) && (i32_tmp <= INT16_MAX));
  ps_scale->i16_y = (int16_t)i32_tmp;

  i32_tmp = (1000 * (int32_t)i16_avg_delta) / ((int32_t)s_avg_delta.i16_z);
  assert((i32_tmp >= INT16_MIN) && (i32_tmp <= INT16_MAX));
  ps_scale->i16_z = (int16_t)i32_tmp;

  return SENSOR_OK;
}
// ==========================| Low level functions |==========================
// =============================| Print related |=============================

e_sensor_error sensor_scan_bus_print_found_devices_priv(void) {
  // If device will be found,
  const char *pac_read = "read";
  const char *pac_write = "write";
  const char *pac_rw;

  // Write flag can be 0 or 1.
  for (uint8_t b_write_flag = 0; b_write_flag < 2; b_write_flag++) {
    // 7 bit address -> 0~127
    for (uint8_t u8_address = 0; u8_address < 128; u8_address++) {
      if (sensor_is_device_on_bus(u8_address, b_write_flag)) {
        if (b_write_flag) {
          pac_rw = pac_write;
        } else {
          pac_rw = pac_read;
        }

        SENSORS_LOGI(tag, "Device found at %d (0x%X) - %s", u8_address,
                     u8_address, pac_rw);
      }
    }
  }

  return SENSOR_OK;
}
// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
