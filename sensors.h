/**
 * @file
 * @author Martin Stejskal
 * @brief Service for sensors
 */
#ifndef __SENSORS_H__
#define __SENSORS_H__
// ===============================| Includes |================================
#include <stdbool.h>

#include "sensors_common.h"
// ================================| Defines |================================

// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Initialize peripheral and sensors
 *
 * Probe known devices and assign driver for every feature.
 *
 * @return SENSOR_OK if no problem.
 */
e_sensor_error sensors_initialize(void);

/**
 * @brief Deinitialize all sensors and peripheral
 * @return SENSOR_OK if no problem.
 */
e_sensor_error sensors_deinitialize(void);

/**
 * @brief Tells whether is sensors logic initialized or not
 * @return True if initialization was pass, false otherwise.
 */
bool sensors_are_initialized(void);

/**
 * @brief Tells if feature is available or not
 * @param e_feature Selected feature
 * @return True if available, false otherwise
 */
bool sensor_is_available(const te_sensor_feature e_feature);

/**
 * @brief Get required data
 * @param e_feature Selected feature
 * @param[in,out] ps_data Set @ref e_data_type for required feature. Basically
 *                you can tell if you want raw, integer or float value. Beware
 *                that some features works with vector and some with 1 value.
 * @return SENSOR_OK if no problem. SENSOR_NOT_SUPPORTED if feature is not
 *         available, SENSOR_INVALID_DATA_TYPE if data type does not match
 *         with selected feature.
 */
e_sensor_error sensor_get(const te_sensor_feature e_feature,
                          ts_sensor_utils_data *ps_data);
// ========================| Middle level functions |=========================
/**
 * @brief Get normalized value from sensor
 *
 * Accelerometer: mG
 * Gyroscope: degrees * 10
 * Magnetometer: uT
 * Temperature: degrees of Celsius * 10
 * Pressure: Pa
 *
 * @param e_feature Selected feature
 * @param[out] ps_data Output variable
 * @return SENSOR_OK if no problem. SENSOR_NOT_SUPPORTED if feature is not
 *         available
 */
e_sensor_error sensor_get_int(const te_sensor_feature e_feature,
                              ts_sensor_utils_data *ps_data);

/**
 * @brief Get normalized value from sensor
 *
 * Accelerometer: mg
 * Gyroscope: degrees
 * Magnetometer: uT
 * Temperature: degrees of Celsius
 * Pressure: Pa
 *
 * @param e_feature Selected feature
 * @param[out] ps_data_float Output variable
 * @return SENSOR_OK if no problem. SENSOR_NOT_SUPPORTED if feature is not
 *         available
 */
e_sensor_error sensor_get_float(const te_sensor_feature e_feature,
                                ts_sensor_utils_data *ps_data_float);

/**
 * @brief Tells whether given feature require calibration
 * @param e_feature Selected feature
 * @param[out] pb_need_calibrate Store information about calibration need
 * @return SENSOR_OK if no problem. SENSOR_NOT_SUPPORTED if feature is not
 *         available
 */
e_sensor_error sensor_need_calibrate(const te_sensor_feature e_feature,
                                     bool *pb_need_calibrate);

/**
 * @brief Calibrate selected feature
 * @param e_feature Selected feature
 * @param e_level Set if you want quick, but inaccurate calibration or you
 *        prefer longer, but more accurate calibration
 * @return SENSOR_OK if no problem. SENSOR_NOT_SUPPORTED if feature is not
 *         available
 */
e_sensor_error sensor_calibrate(const te_sensor_feature e_feature,
                                const ts_sensor_calibration e_level);
// ==========================| Low level functions |==========================
/**
 * @brief Get raw value from sensor
 *
 * This might be useful for quick processing, since value is not recalculated
 * to normalized value.
 *
 * @param e_feature Selected feature
 * @param[out] ps_data Output data will be pushed there
 * @return SENSOR_OK if no problem. SENSOR_NOT_SUPPORTED if feature is not
 *         available
 */
e_sensor_error sensor_get_raw(const te_sensor_feature e_feature,
                              ts_sensor_utils_data *ps_data);

// =============================| Print related |=============================
/**
 * @brief Scan I2C bus and print which devices were found
 *
 * Function just scan all possible address on I2C bus and report found devices.
 * This might be handy when debugging.
 *
 * @return SENSOR_NOT_INITIALIZED if module was not initialized yet. Otherwise
 *         it returns SENSOR_OK.
 */
e_sensor_error sensor_scan_bus_print_found_devices(void);

#endif  // __SENSORS_H__
