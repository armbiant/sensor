/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration for ESP architecture
 */
#ifndef __ESP_SENSORS_CFG_H__
#define __ESP_SENSORS_CFG_H__
// ===============================| Includes |================================
// Following allow to load "global" settings from one file
// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#if __has_include("cfg.h")
#include "cfg.h"
#endif
#endif
// ================================| Defines |================================

// ============================| Default values |=============================
///@todo Move most of those things as argument while initializing sensors

#ifndef SENSORS_I2C_INTERFACE
/**
 * @brief Selects I2C interface on chip
 */
#define SENSORS_I2C_INTERFACE (I2C_NUM_0)
#endif  // SENSORS_I2C_INTERFACE

#ifndef SENSORS_I2C_IO_SDA
/**
 * @brief Used GPIO for SDA
 */
#define SENSORS_I2C_IO_SDA (21)
#endif  // SENSORS_I2C_IO_SDA

#ifndef SENSORS_I2C_IO_SCL
/**
 * @brief Used GPIO for SCL
 */
#define SENSORS_I2C_IO_SCL (22)
#endif  // SENSORS_I2C_IO_SCL

#ifndef SENSOR_DEFAULT_I2C_SCL_SPEED_HZ
/**
 * @brief Default I2C speed in HZ
 */
#define SENSOR_DEFAULT_I2C_SCL_SPEED_HZ (100000)
#endif  // SENSOR_DEFAULT_I2C_SCL_SPEED_HZ
// ==========================| Preprocessor checks |==========================

#endif  // __ESP_SENSORS_CFG_H__
