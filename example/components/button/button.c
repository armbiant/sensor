/**
 * @file
 * @author Martin Stejskal
 * @brief Simple button handler
 */
// ===============================| Includes |================================
#include "button.h"

#include <driver/gpio.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>
#include <freertos/task.h>

// ================================| Defines |================================
// Keep Eclipse happy
#ifndef IRAM_ATTR
#define IRAM_ATTR
#endif  // IRAM_ATTR

// Value signalize invalid GPIO
#define BTN_INVALID_GPIO (0xFF)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
typedef struct {
  uint8_t u8_gpio_num;
  uint32_t u32_timestamp_ms;
} ts_button_event;

typedef struct {
  // Event queue
  xQueueHandle gpio_evnt_queue;

  // Callback for event "button pressed"
  tf_button_pressed_cb pf_button_pressed_cb;
  uint8_t u8_btn_pin;
} tsRuntime;
// ===========================| Global variables |============================

/**
 * @brief Runtime variables under one variable
 */
static tsRuntime ms_runtime;

static char* tag = "Button manager";
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
static void IRAM_ATTR _gpio_isr_handler(void* pv_arg);

static void _exitTask(void);
// =========================| High level functions |==========================
void button_task_rtos(void* pv_args) {
  // If any argument is given, it should be processed
  if (pv_args) {
    const ts_btn_args* ps_args = (ts_btn_args*)pv_args;

    button_set_cb(ps_args->pf_callback);
    button_set_gpio(ps_args->u8_btn_pin);
  } else {
    // Clean up callback
    ms_runtime.pf_button_pressed_cb = 0;
    ms_runtime.u8_btn_pin = BTN_INVALID_GPIO;
  }

  // Data in queue are copied -> need to allocate storage
  ts_button_event s_event;
  uint32_t u32_button_pressed_time_ms = 0;

  // GPIO values are passed as 32 bit values for some reason
  ms_runtime.gpio_evnt_queue =
      xQueueCreate(BUTTON_EVENT_QUEUE_SIZE_ITEMS, sizeof(ts_button_event));

  while (1) {
    if (xQueueReceive(ms_runtime.gpio_evnt_queue, &s_event, portMAX_DELAY)) {
      ESP_LOGW(tag, "GPIO: %d Timestamp: %d", s_event.u8_gpio_num,
               s_event.u32_timestamp_ms);

      if ((s_event.u32_timestamp_ms - u32_button_pressed_time_ms) >
          BUTTON_DEBOUNCE_TIME_MS) {
        // Valid button press
        ESP_LOGI(tag, "Button press at GPIO %d", s_event.u8_gpio_num);

        // If callback is set, call it. Otherwise nothing to do
        if (ms_runtime.pf_button_pressed_cb) {
          ms_runtime.pf_button_pressed_cb();
        } else {
          ESP_LOGI(tag, "Callback not set. Nothing to do");
        }

      } else {
        // De-bouncing -> do nothing
        ESP_LOGV(tag, "debouncing...");
      }

      u32_button_pressed_time_ms = s_event.u32_timestamp_ms;

    } else {
      // Timeout - wait again
    }
  }

  _exitTask();
}
// ========================| Middle level functions |=========================
void button_set_cb(const tf_button_pressed_cb pf_button_pressed) {
  ms_runtime.pf_button_pressed_cb = pf_button_pressed;
}

void button_set_gpio(const uint8_t u8_btn_pin) {
  // Input button connects to GND. Therefore need pull up and react to
  // falling edge

  gpio_config_t s_gpio_cfg = {
      .pin_bit_mask = (1ULL << u8_btn_pin),
      .mode = GPIO_MODE_INPUT,
      .pull_up_en = 1,
      .pull_down_en = 0,
      .intr_type = GPIO_INTR_NEGEDGE,

  };

  gpio_config(&s_gpio_cfg);

  // Use default flags
  gpio_install_isr_service(0);

  // Store selected GPIO into runtime variable - be available in interrupt
  // handler
  ms_runtime.u8_btn_pin = u8_btn_pin;
  gpio_isr_handler_add(u8_btn_pin, _gpio_isr_handler,
                       (void*)&ms_runtime.u8_btn_pin);
}
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================

static void IRAM_ATTR _gpio_isr_handler(void* pv_arg) {
  // Need to register also timestamp "now", because it might take some time
  // before RTOS get into processing this (due to other blocking function for
  // example)
  ts_button_event s_event;

  s_event.u8_gpio_num = *((uint8_t*)pv_arg);
  s_event.u32_timestamp_ms = esp_log_timestamp();

  xQueueSendFromISR(ms_runtime.gpio_evnt_queue, &s_event, NULL);
}

static void _exitTask(void) {
  gpio_isr_handler_remove(ms_runtime.u8_btn_pin);

  vTaskDelete(NULL);

  // Just make compiler happy, when logging is disabled
  (void)tag;
}
