/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for Anomaly meter
 *
 * Mainly here is HW configuration
 */
#ifndef __CFG_H__
#define __CFG_H__
// ===============================| Includes |================================
#include <esp_log.h>
// ================================| Defines |================================
/**
 * @brief Protocol CPU
 */
#define PRO_CPU (0)

/**
 * @brief Application CPU
 */
#define APP_CPU (1)
// ==================================| I/O |==================================
/**
 * @brief I2C GPIO mapping
 *
 * @{
 */
///@brief I2C data
#define IO_I2C_SDA (21)
#define SENSORS_I2C_IO_SDA (IO_I2C_SDA)

///@brief I2C clock
#define IO_I2C_SCL (22)
#define SENSORS_I2C_IO_SCL (IO_I2C_SCL)
/**
 * @}
 */

/**
 * @brief Button mapping to GPIO pin
 *
 */
#define IO_BUTTON (27)
// ===============================| Advanced |================================
// ============================| Default values |=============================
/**
 * @brief Define function which returns ms (32 bit value) as time reference
 */
#define GET_CLK_MS_32BIT() esp_log_timestamp()

// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
#endif  // __CFG_H__
