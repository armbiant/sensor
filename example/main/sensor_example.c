/**
 * @file
 * @author Martin Stejskal
 * @brief Main user function - mainly for creating tasks
 */
// ===============================| Includes |================================
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/semphr.h>
#include <freertos/task.h>

#include "button.h"
#include "cfg.h"
#include "sensors.h"
#include "sensors_common.h"

// ================================| Defines |================================
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================
/**
 * @brief Tag for logging system
 */
static char *tag = "main";

/**
 * @brief Semaphore handler which keeps track whether sensors are used or not
 */
static SemaphoreHandle_t mh_sensors_active;
// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
static void _read_from_sensors_task(void *pv_args);
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
static void _action_button_pressed(void);
// =========================| High level functions |==========================
void app_main(void) {
  ESP_LOGI(tag, "Sensor example alive!");

  // Create semaphore
  mh_sensors_active = xSemaphoreCreateBinary();
  xSemaphoreGive(mh_sensors_active);

  // This task does not require much attention, so we can use application core
  // and low priority
  xTaskCreatePinnedToCore(_read_from_sensors_task, "Sensors", 8 * 1024, NULL,
                          tskIDLE_PRIORITY, NULL, APP_CPU);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
static void _read_from_sensors_task(void *pv_args) {
  sensors_initialize();

  // Scan I2C bus - just for easier debugging/testing
  ESP_LOGI(tag, "Probing I2C bus...");
  sensor_scan_bus_print_found_devices();

  // Once sensors are initialized, assign button to calibration function
  const ts_btn_args s_btn_args = {.pf_callback = _action_button_pressed,
                                  .u8_btn_pin = IO_BUTTON};

  xTaskCreatePinnedToCore(button_task_rtos, "Button", 4 * 1024,
                          (void *)&s_btn_args, tskIDLE_PRIORITY, NULL, APP_CPU);

  e_sensor_error e_err_code;
  ts_sensor_utils_data s_data;

  while (1) {
    // Wait until sources are released
    xSemaphoreTake(mh_sensors_active, portMAX_DELAY);

    // Print extra lines, so output will be easier to process by human
    printf("\n\n");

    // Read from all sensors, if supported
    for (te_sensor_feature e_feature = (te_sensor_feature)0;
         e_feature < SENSOR_FEATURE_MAX; e_feature++) {
      e_err_code = sensor_get_int(e_feature, &s_data);

      if (e_err_code) {
        ESP_LOGW(tag, "%s %s (code %d)", sensor_feature_to_str(e_feature),
                 sensor_error_code_to_str(e_err_code), e_err_code);
      } else {
        ESP_LOGI(tag, "%s", sensor_data_to_str(&s_data));
      }
    }

    xSemaphoreGive(mh_sensors_active);

    vTaskDelay(5000 / portTICK_PERIOD_MS);
  }
}
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
static void _action_button_pressed(void) {
  // Need to "remember" last feature value between calls
  static te_sensor_feature e_feature = (te_sensor_feature)0;

  bool b_need_calibrate = false;
  bool b_at_least_1_need_calibrate = false;
  e_sensor_error e_err_code;

  // Check if at least 1 feature needs calibration. If so, it make sense to
  // rotate over features and calibrate them. Otherwise it does not make sense
  // at all and we can quit right away
  for (te_sensor_feature e_tmp_feature = (te_sensor_feature)0;
       e_tmp_feature < SENSOR_FEATURE_MAX; e_tmp_feature++) {
    sensor_need_calibrate(e_tmp_feature, &b_need_calibrate);

    b_at_least_1_need_calibrate |= b_need_calibrate;

    if (b_at_least_1_need_calibrate) {
      break;
    }
  }

  if (!b_at_least_1_need_calibrate) {
    ESP_LOGW(tag, "Current sensor does not need/support any calibration");
    return;
  }

  // So we know that at least 1 calibration function is available. So simply
  // search for feature, that support calibration
  b_need_calibrate = false;
  while (!b_need_calibrate) {
    sensor_need_calibrate(e_feature, &b_need_calibrate);

    // Either way, move to the next feature. It will be used in next cycle or
    // next function call
    e_feature++;
    if (e_feature >= SENSOR_FEATURE_MAX) {
      e_feature = (te_sensor_feature)0;
    }
  }

  // Since feature contain "next" value, it is required to deduct it by 1 in
  // order to get current value
  ESP_LOGI(tag, "Calibrating %s", sensor_feature_to_str(e_feature - 1));

  // Wait until sources are released
  xSemaphoreTake(mh_sensors_active, portMAX_DELAY);

  e_err_code = sensor_calibrate(e_feature - 1, SENSOR_CALIB_NORMAL);

  xSemaphoreGive(mh_sensors_active);

  if (e_err_code) {
    ESP_LOGE(tag, "Calibration of %s feature failed with: %s",
             sensor_feature_to_str(e_feature),
             sensor_error_code_to_str(e_err_code));
  } else {
    ESP_LOGI(tag, "Feature %s calibrated :)",
             sensor_feature_to_str(e_feature - 1));
  }
}
