/**
 * @file
 * @author Martin Stejskal
 * @brief HAL for MPU9250
 */
#ifndef __MPU9250_H__
#define __MPU9250_H__
// ===============================| Includes |================================
#include "sensors_common.h"
// ================================| Defines |================================
/**
 * @brief List of I2C address for MPU9250
 *
 * @{
 */
#define MPU9250_I2C_ADDR_ACC_GYRO_LIST \
  { 0x68, 0x69 }

#define MPU9250_I2C_ADDR_MAG (0x0C)
/**
 * @}
 */

/**
 * @brief Gyroscope resolution (LSB per degree)
 *
 * 0.00763 degree per LSB
 */
#define MPU9250_GYRO_RES (131)

/**
 * @brief Accelerometer resolution (LSB per 1g)
 *
 * 61.04 ug per LSB
 */
#define MPU9250_ACC_RES (16384)

/**
 * @brief Magnetometer resolution (LSB per 1T)
 *
 * 0.6uT per LSB
 */
#define MPU9250_MAG_RES (1666667)

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Returns descriptor for this sensor
 * @return Pointer to driver descriptor
 */
const ts_sensor_descriptor *mpu9250_get_descriptor(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __MPU9250_H__
