/**
 * @file
 * @author Martin Stejskal
 * @brief HAL for MPU6050 chip
 */
#ifndef __MPU6050_H__
#define __MPU6050_H__
// ===============================| Includes |================================
#include "sensors_common.h"
// ================================| Defines |================================
/**
 * @brief List of I2C address for MPU6050
 */
#define MPU6050_I2C_ADDR_LIST \
  { 0x68, 0x69 }

/**
 * @brief Some unnatural value when checking if temperature sensor is ready
 *
 * When temperature sensor is initializing, value in register is zero and due
 * to nature of formula, result is non-zero value. So when sensor is simply
 * not ready yet, function return something ridiculous to make it clean, that
 * this is not typical.
 */
#define MPU6050_INVALID_TEMP (-123)

/**
 * @brief Gyroscope resolution (number of LSB per degree)
 */
#define MPU6050_GYRO_RES (131)

/**
 * @brief Accelerometer resolution (number of LSB per 1 G)
 */
#define MPU6050_ACC_RES (16384)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// =======================| Structures, enumerations |========================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Returns descriptor for this sensor
 * @return Pointer to driver descriptor
 */
const ts_sensor_descriptor *mpu6050_get_descriptor(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __MPU6050_H__
