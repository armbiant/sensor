/**
 * @file
 * @author Martin Stejskal
 * @brief Common routines & macros for sensor logic
 *
 * Mainly used by drivers, but also upper layers can use those functions.
 * This header should be architecture independent, but it may include
 * architecture dependent headers in order to properly define function mapping.
 */
// ===============================| Includes |================================
#include "sensors_common.h"

#include <assert.h>
#include <stdio.h>

// ================================| Defines |================================

// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
#if SENSOR_FEATURE_MAX > 32
#error "Expected 32 features at max. It is necessary to redesign code"
#endif
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *tag = "Sensors common";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static const char *_get_unknown(void);
// =========================| High level functions |==========================

// ========================| Middle level functions |=========================

// ==========================| Low level functions |==========================
void sensor_swap_bytes_16_bit(tu_sensor_16_bit *pu_variable) {
  uint8_t u8temp = pu_variable->s.u8_high;
  pu_variable->s.u8_high = pu_variable->s.u8_low;
  pu_variable->s.u8_low = u8temp;
}

void sensor_multiply_and_divide_int_vector(ts_sensor_vect *ps_vect,
                                           int16_t i16_multiplier,
                                           int16_t i16_divider) {
  // Due to multiplication need 2x wider accumulator
  int32_t i32_tmp_x, i32_tmp_y, i32_tmp_z;

  i32_tmp_x = ((int32_t)i16_multiplier * (int32_t)ps_vect->i16_x) /
              (int32_t)i16_divider;
  i32_tmp_y = ((int32_t)i16_multiplier * (int32_t)ps_vect->i16_y) /
              (int32_t)i16_divider;
  i32_tmp_z = ((int32_t)i16_multiplier * (int32_t)ps_vect->i16_z) /
              (int32_t)i16_divider;

  // If overflow happened, cut value on it's maximum at least to avoid
  // unexpected results when 32 bit value would be cut into 16 bit value
  if (i32_tmp_x > INT16_MAX) {
    i32_tmp_x = INT16_MAX;
  }
  if (i32_tmp_x < INT16_MIN) {
    i32_tmp_x = INT16_MIN;
  }

  if (i32_tmp_y > INT16_MAX) {
    i32_tmp_y = INT16_MAX;
  }
  if (i32_tmp_y < INT16_MIN) {
    i32_tmp_y = INT16_MIN;
  }

  if (i32_tmp_z > INT16_MAX) {
    i32_tmp_z = INT16_MAX;
  }
  if (i32_tmp_z < INT16_MIN) {
    i32_tmp_z = INT16_MIN;
  }

  ps_vect->i16_x = i32_tmp_x;
  ps_vect->i16_y = i32_tmp_y;
  ps_vect->i16_z = i32_tmp_z;
}

uint16_t sensor_get_recommended_num_of_iterations(
    const te_sensor_feature e_feature, const ts_sensor_calibration e_level) {
  // Not all features support calibration. For those ones is returned zero.
  // Values are empiric
  switch (e_feature) {
    case SENSOR_FEATURE_GYROSCOPE:

      // Values are kind of empiric
      switch (e_level) {
        case SENSOR_CALIB_SUPER_FAST:
          return 10;
        case SENSOR_CALIB_FAST:
          return 100;
        case SENSOR_CALIB_NORMAL:
          return 1000;
        case SENSOR_CALIB_SLOW:
          return 5000;
        case SENSOR_CALIB_SUPER_SLOW:
          return 10000;
        case SENSOR_CALIB_SLOTH:
          return 50000;
        default:
          // It should not happen
          assert(0);
          return 1000;
      }

    case SENSOR_FEATURE_MAGENETOMETER:
      switch (e_level) {
        case SENSOR_CALIB_SUPER_FAST:
          return 100;
        case SENSOR_CALIB_FAST:
          return 300;
        case SENSOR_CALIB_NORMAL:
          return 800;
        case SENSOR_CALIB_SLOW:
          return 1500;
        case SENSOR_CALIB_SUPER_SLOW:
          return 2700;
        case SENSOR_CALIB_SLOTH:
          return 3500;
        default:
          // It should not happen
          assert(0);
          return 800;
      }

    default:
      // Can not calibrate or do not know how to calibrate
      return 0;
  }
}
// =============================| Print related |=============================
const char *sensor_data_to_str(const ts_sensor_utils_data *ps_data) {
  // Do not expect to exceed 80 characters
  static char ac_out[80];

  const te_sensor_data_type e_data_type = ps_data->e_data_type;

  if ((e_data_type == SENSOR_DTYPE_RAW) || (e_data_type == SENSOR_DTYPE_INT)) {
    // Single value - integer
    sprintf(ac_out, "%s : %d [%s]", sensor_feature_to_str(ps_data->e_feature),
            ps_data->u_d.i16, sensor_get_unit_str(ps_data));

  } else if (e_data_type == SENSOR_DTYPE_FLOAT) {
    // Single value - float
    sprintf(ac_out, "%s : %f [%s]", sensor_feature_to_str(ps_data->e_feature),
            ps_data->u_d.f, sensor_get_unit_str(ps_data));

  } else if ((e_data_type == SENSOR_DTYPE_RAW_VECT) ||
             (e_data_type == SENSOR_DTYPE_INT_VECT)) {
    // Vector - integer
    sprintf(ac_out, "%s : X: %d Y: %d Z: %d  [%s]",
            sensor_feature_to_str(ps_data->e_feature),
            ps_data->u_d.s_vect.i16_x, ps_data->u_d.s_vect.i16_y,
            ps_data->u_d.s_vect.i16_z, sensor_get_unit_str(ps_data));

  } else if (e_data_type == SENSOR_DTYPE_FLOAT_VECT) {
    // Vector - float
    sprintf(ac_out, "%s : X: %f Y: %f Z: %f [%s]",
            sensor_feature_to_str(ps_data->e_feature),
            ps_data->u_d.s_vect_float.f_x, ps_data->u_d.s_vect_float.f_y,
            ps_data->u_d.s_vect_float.f_z, sensor_get_unit_str(ps_data));

  } else {
    // Unknown combination
    sprintf(ac_out, "Unknown data type");
  }

  return ac_out;
}

const char *sensor_error_code_to_str(const e_sensor_error e_err_code) {
  switch (e_err_code) {
    case SENSOR_OK:
      return "OK";
    case SENSOR_FAIL:
      return "Fail";
    case SENSOR_NOT_SUPPORTED:
      return "Not supported";
    case SENSOR_NOT_INITIALIZED:
      return "Not initialized";
    case SENSOR_DEVICE_NOT_FOUND:
      return "Device not found";
    case SENSOR_INVALID_PARAM:
      return "Invalid parameter";
    case SENSOR_INVALID_DATA_TYPE:
      return "Invalid data type";
    case SENSOR_PERIPHERAL_INIT_FAIL:
      return "Peripheral initialization failed";
    case SENSOR_PERIPHERAL_DEINIT_FAIL:
      return "Peripheral deinitialization failed";
    case SENSOR_INVALID_STATE:
      return "Invalid state";
    case SENSOR_ALREADY_RUNNING:
      return "Already running";
    case SENSOR_NOT_RUNNING:
      return "Not running";
    case SENSOR_TIMER_INITIALIZATION_FAILED:
      return "Timer initialization failed";
    case SENSOR_TIMER_DEINITIALIZATION_FAILED:
      return "Timer deinitialization failed";
    case SENSOR_EMPTY_POINTER:
      return "Empty pointer";
    case SENSOR_INTERNAL_ERROR:
      return "Internal error";
    case SENSOR_TIMEOUT:
      return "Timeout";
    case SENSOR_UNEXPECTED_EXTERNAL_CONDITIONS:
      return "Unexpected external conditions";
    case SENSOR_INVALID_VALUE:
      return "Invalid value";
  }

  /* Every other possible case. Do not use "default" in switch in order to
   * generate compiler warning when all values from enumeration are defined
   */
  return _get_unknown();
}

const char *sensor_feature_to_str(const te_sensor_feature e_feature) {
  const char *pac_acc = "Accelerometer";
  const char *pac_gyro = "Gyroscope";
  const char *pac_mag = "Magnetometer";
  const char *pac_temp = "Temperature";
  const char *pac_press = "Pressure";
  const char *pac_unknown = "???";

  switch (e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      return pac_acc;
    case SENSOR_FEATURE_GYROSCOPE:
      return pac_gyro;
    case SENSOR_FEATURE_MAGENETOMETER:
      return pac_mag;
    case SENSOR_FEATURE_TEMPERATURE:
      return pac_temp;
    case SENSOR_FEATURE_PRESSURE:
      return pac_press;
    case SENSOR_FEATURE_MAX:
      // This is invalid value. Should not happen
      break;
  }

  // Should not happen
  assert(0);
  return pac_unknown;
}

const char *sensor_data_type_to_str(const te_sensor_data_type e_data_type) {
  switch (e_data_type) {
    case SENSOR_DTYPE_RAW:
      return "Raw";
    case SENSOR_DTYPE_RAW_VECT:
      return "Raw vector";
    case SENSOR_DTYPE_INT:
      return "Integer";
    case SENSOR_DTYPE_INT_VECT:
      return "Integer vector";
    case SENSOR_DTYPE_FLOAT:
      return "Float";
    case SENSOR_DTYPE_FLOAT_VECT:
      return "Float vector";
  }

  return _get_unknown();
}

const char *sensor_get_unit_str(const ts_sensor_utils_data *ps_data) {
  const te_sensor_data_type e_data_type = ps_data->e_data_type;

  // Shortcut - if value is raw, then no units should be printed
  if ((e_data_type == SENSOR_DTYPE_RAW) ||
      (e_data_type == SENSOR_DTYPE_RAW_VECT)) {
    return "-";
  }

  switch (ps_data->e_feature) {
    case SENSOR_FEATURE_ACCELEROMETER:
      if ((e_data_type == SENSOR_DTYPE_INT) ||
          (e_data_type == SENSOR_DTYPE_INT_VECT)) {
        return "mG";
      } else {
        // Expected float
        return "G";
      }
      break;

    case SENSOR_FEATURE_GYROSCOPE:
      if ((e_data_type == SENSOR_DTYPE_INT) ||
          (e_data_type == SENSOR_DTYPE_INT_VECT)) {
        return "deg/sec";
      } else {
        // Expected float
        return "deg/sec";
      }
      break;

    case SENSOR_FEATURE_MAGENETOMETER:
      if ((e_data_type == SENSOR_DTYPE_INT) ||
          (e_data_type == SENSOR_DTYPE_INT_VECT)) {
        return "uT";
      } else {
        // Expected float
        return "T";
      }
      break;
    case SENSOR_FEATURE_TEMPERATURE:
      if ((e_data_type == SENSOR_DTYPE_INT) ||
          (e_data_type == SENSOR_DTYPE_INT_VECT)) {
        return "C x 10";
      } else {
        // Expected float
        return "C";
      }
      break;
    case SENSOR_FEATURE_PRESSURE:
      if ((e_data_type == SENSOR_DTYPE_INT) ||
          (e_data_type == SENSOR_DTYPE_INT_VECT)) {
        return "Pa x 10";
      } else {
        // Expected float
        return "Pa";
      }
      break;
    case SENSOR_FEATURE_MAX:
      // This should not happen
      break;
  }

  // Unknown feature. Can not estimate units
  assert(0);
  return _get_unknown();
}

// ==========================| Internal functions |===========================
static const char *_get_unknown(void) {
  const char *pac_unknown = "???";

  // Just make compile happy when logging is disabled/not used
  (void)tag;

  return pac_unknown;
}
